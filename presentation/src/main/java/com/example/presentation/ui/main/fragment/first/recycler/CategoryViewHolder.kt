package com.example.presentation.ui.main.fragment.first.recycler

import android.util.Log
import android.view.View
import android.widget.TextView
import com.example.presentation.R
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
class CategoryViewHolder(itemView: View) : GroupViewHolder(itemView) {
    private val osName: TextView = itemView.findViewById<View>(R.id.categoryTextView) as TextView
    override fun expand() {
        osName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);
        Log.i("Adapter", "expand")
    }

    override fun collapse() {
        Log.i("Adapter", "collapse")
                osName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_right_black_24dp, 0);
    }

    fun setGroupName(group: ExpandableGroup<*>) {
        osName.text = group.title
    }

}