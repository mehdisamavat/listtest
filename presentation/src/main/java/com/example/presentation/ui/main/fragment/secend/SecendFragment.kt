package com.example.presentation.ui.main.fragment.secend

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.example.presentation.R
import kotlinx.android.synthetic.main.toolbar.*

class SecendFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.secend_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        backArrow.setOnClickListener { findNavController().navigate(R.id.action_secendFragment_to_FirstFragment) }
        backTextView.setOnClickListener { findNavController().navigate(R.id.action_secendFragment_to_FirstFragment) }

    }


}
