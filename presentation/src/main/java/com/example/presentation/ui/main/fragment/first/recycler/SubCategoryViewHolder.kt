package com.example.presentation.ui.main.fragment.first.recycler

import android.view.View
import android.widget.TextView
import com.example.domain.entity.SubCategoryObject
import com.example.presentation.R
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder


/**
 * Created by Mehdi Samavat on 5/18/20.
 */
class SubCategoryViewHolder(itemView: View) : ChildViewHolder(itemView) {


    private val phoneName: TextView = itemView.findViewById<View>(R.id.subCategoryTextView) as TextView
    fun onBind(subCategoryObject: SubCategoryObject, group: ExpandableGroup<*>) {
        phoneName.text = subCategoryObject.title
        phoneName . setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_right_black_24dp, 0)
    }
}
