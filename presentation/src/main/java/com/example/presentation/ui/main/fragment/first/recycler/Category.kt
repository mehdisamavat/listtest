package com.example.presentation.ui.main.fragment.first.recycler

import android.annotation.SuppressLint
import com.example.domain.entity.SubCategoryObject
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
@SuppressLint("ParcelCreator")
class Category(title: String?, items: List<SubCategoryObject?>?) :
    ExpandableGroup<SubCategoryObject?>(title, items)