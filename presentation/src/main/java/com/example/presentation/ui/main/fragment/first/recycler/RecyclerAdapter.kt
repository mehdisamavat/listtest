package com.example.presentation.ui.main.fragment.first.recycler

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.presentation.R
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


/**
 * Created by Mehdi Samavat on 5/18/20.
 */
class RecyclerAdapter(
    private val activity: Activity,
    groups: List<ExpandableGroup<*>?>?
) : ExpandableRecyclerViewAdapter<CategoryViewHolder, SubCategoryViewHolder>(groups) {
    private val clickSubject = PublishSubject.create<Int>()
    val clickEvent: Observable<Int> = clickSubject
    override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val inflater =
            activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.category, parent, false)
        return CategoryViewHolder(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup, viewType: Int): SubCategoryViewHolder {
        val inflater = activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.subcategory, parent, false)
        return SubCategoryViewHolder(view)
    }

    override fun onBindChildViewHolder(
        holder: SubCategoryViewHolder,
        flatPosition: Int,
        group: ExpandableGroup<*>,
        childIndex: Int
    ) {
        val subCategoryObject = (group as Category).items[childIndex]!!
        holder.onBind(subCategoryObject, group)

        holder.itemView.setOnClickListener { clickSubject.onNext(0) }
    }

    override fun onBindGroupViewHolder(
        holder: CategoryViewHolder,
        flatPosition: Int,
        group: ExpandableGroup<*>?
    ) {
        holder.setGroupName(group!!)
    }

}