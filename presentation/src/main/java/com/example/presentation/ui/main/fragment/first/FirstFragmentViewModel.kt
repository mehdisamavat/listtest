package com.example.presentation.ui.main.fragment.first

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.entity.ResultObject
import com.example.domain.usecase.category.GetCategoryUseCase
import com.example.domain.usecase.invoke
import com.example.presentation.common.view.BaseViewModel
import com.example.presentation.common.model.Resource
import com.example.presentation.common.model.ResourceState
import javax.inject.Inject

/**
 * Created by Mehdi Samavat on 5/18/20.
 */

class FirstFragmentViewModel @Inject constructor(private val getCategoryUseCase: GetCategoryUseCase) :
    ViewModel() {

    val category: MutableLiveData<Resource<List<ResultObject>>> = MutableLiveData()
    var manufacturerPagedResult: List<ResultObject>? = null


    @SuppressLint("CheckResult")
    fun getCategory() {
        category.value = Resource(ResourceState.LOADING)
        getCategoryUseCase.invoke().subscribe({
            category.value = Resource(ResourceState.SUCCESS, it.resultObject)
            Log.i("mehdi", it.toString())
        }, {
            category.value=Resource(ResourceState.ERROR, failure = it)

            Log.i("mehdi", "$it")
        })
    }
}