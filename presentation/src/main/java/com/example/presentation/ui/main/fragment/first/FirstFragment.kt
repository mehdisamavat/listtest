package com.example.presentation.ui.main.fragment.first

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.domain.entity.ResultObject
import com.example.presentation.common.view.BaseFragment
import com.example.presentation.R
import com.example.presentation.common.di.ViewModelProviderFactory
import com.example.presentation.common.extention.mapper.toCategory
import com.example.presentation.common.extention.observe
import com.example.presentation.common.extention.viewModelProvider
import com.example.presentation.common.model.Resource
import com.example.presentation.common.model.ResourceState
import com.example.presentation.ui.main.fragment.first.recycler.RecyclerAdapter
import kotlinx.android.synthetic.main.fragment_first.*
import kotlinx.android.synthetic.main.loading_view.*
import javax.inject.Inject


class FirstFragment : BaseFragment() {


    @Inject
    lateinit var factory: ViewModelProviderFactory
    lateinit var viewModel: FirstFragmentViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModelProvider(factory)
        if (viewModel.manufacturerPagedResult != null) {
            updateUi(viewModel.manufacturerPagedResult!!)
        } else viewModel.getCategory()

        observe(viewModel.category, ::handleStates)
    }

    private fun handleStates(resource: Resource<List<ResultObject>>?) {
        resource?.let {
            when (resource.resourceState) {
                ResourceState.LOADING -> loadingView.visibility = View.VISIBLE
                ResourceState.SUCCESS -> updateUi(it.data!!)
                ResourceState.ERROR -> {
                    loadingView.visibility = View.GONE
                    showMessage(it.failure?.message.toString())
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    fun updateUi(reult: List<ResultObject>) {
        viewModel.manufacturerPagedResult = reult
        val layoutManager = LinearLayoutManager(requireContext())
        recyclerViewExpandable.layoutManager = layoutManager
        val adapter = RecyclerAdapter(requireActivity(), reult.map { it.toCategory() })
        recyclerViewExpandable.adapter = adapter
        adapter.clickEvent.subscribe(
            { findNavController().navigate(R.id.action_FirstFragment_to_secendFragment) },
            {})
        loadingView.visibility = View.GONE
    }
}
