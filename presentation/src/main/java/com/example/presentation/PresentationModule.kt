package com.example.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.domain.executor.JobExecutor
import com.example.domain.executor.PostExecutionThread
import com.example.domain.executor.ThreadExecutor
import com.example.presentation.common.di.ViewModelKey
import com.example.presentation.common.di.ViewModelProviderFactory
import com.example.presentation.common.executor.UIThread
import com.example.presentation.ui.main.fragment.first.FirstFragment
import com.example.presentation.ui.main.fragment.first.FirstFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 * Created by Mehdi Samavat on 5/18/20.
 */

@Module
abstract class PresentationModule{

    @Binds
    abstract fun bindViewModelFactory(viewModelProviderFactory: ViewModelProviderFactory): ViewModelProvider.Factory

    @ContributesAndroidInjector
    abstract fun firstFragmentInjector(): FirstFragment

    @Binds
    @IntoMap
    @ViewModelKey(FirstFragmentViewModel::class)
    abstract fun viewModel(firstFragmentViewModel: FirstFragmentViewModel): ViewModel


    @Binds
    abstract fun postExecutionThread(thread: UIThread): PostExecutionThread


    @Binds
    abstract fun threadExecutor(executor: JobExecutor): ThreadExecutor



}