package com.example.presentation.common.view

import android.widget.Toast
import androidx.annotation.StringRes
import dagger.android.support.DaggerFragment


/**
 * Created by Mehdi Samavat on 5/18/20.
 */
abstract class BaseFragment : DaggerFragment() {



    fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }




}