package com.example.presentation.common.extention.mapper

import com.example.domain.entity.ResultObject
import com.example.presentation.ui.main.fragment.first.recycler.Category

/**
 * Created by Mehdi Samavat on 5/19/20.
 */

fun ResultObject.toCategory():Category= Category(title, subCategoryObjects)