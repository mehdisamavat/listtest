package com.example.presentation.common.view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.presentation.BuildConfig

import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import org.xml.sax.ErrorHandler
import javax.inject.Inject

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
abstract class BaseViewModel : ViewModel() {

    private val disposable = CompositeDisposable()

    protected fun Disposable.track() {
        disposable.add(this)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}