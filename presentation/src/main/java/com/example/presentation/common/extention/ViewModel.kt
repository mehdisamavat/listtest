package com.example.presentation.common.extention

import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.example.presentation.common.di.ViewModelProviderFactory


internal fun <T> LifecycleOwner.observe(liveData: LiveData<T>?, action: (t: T) -> Unit) {
    liveData?.observe(this, Observer { t -> action(t) })
}

internal inline fun <reified VM : ViewModel> Fragment.viewModelProvider(provider: ViewModelProviderFactory) =
    ViewModelProviders.of(this, provider)[VM::class.java]