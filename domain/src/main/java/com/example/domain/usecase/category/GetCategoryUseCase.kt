package com.example.domain.usecase.category

import com.example.domain.entity.ResponseObject
import com.example.domain.executor.transformer.STransformer
import com.example.domain.repository.CategoryRepository
import com.example.domain.usecase.UseCaseSingle
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
class GetCategoryUseCase @Inject constructor(
    private val categoryRepository: CategoryRepository,
    private val sTransformer: STransformer<ResponseObject>
) : UseCaseSingle<ResponseObject, Unit>() {

    override fun execute(param: Unit): Single<ResponseObject> {
        return categoryRepository.getCategory().compose(sTransformer)
    }
}