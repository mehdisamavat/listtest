package com.example.domain

import com.example.domain.entity.ResponseObject
import com.example.domain.executor.JobExecutor
import com.example.domain.executor.PostExecutionThread
import com.example.domain.executor.ThreadExecutor
import com.example.domain.executor.transformer.AsyncSTransformer
import com.example.domain.executor.transformer.STransformer
import com.example.domain.repository.CategoryRepository
import dagger.Binds
import dagger.Module
import dagger.Reusable

/**
 * Created by Mehdi Samavat on 5/17/20.
 */
@Module
abstract class DomainModule {


    @Binds
    abstract fun detailTransformer(
        transformer: AsyncSTransformer<ResponseObject>
    ): STransformer<ResponseObject>

}