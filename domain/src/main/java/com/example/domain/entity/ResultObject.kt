package com.example.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class ResultObject(
    val id: String,
    val subCategoryObjects: List<SubCategoryObject>,
    val title: String)