package com.example.domain.entity

data class ResponseObject(
    val message: Any?=null,
    val paging: Any?=null,
    val resultObject: List<ResultObject>,
    val statusCode: String,
    val succeeded: Boolean,
    val uiMessage: Any?=null
)