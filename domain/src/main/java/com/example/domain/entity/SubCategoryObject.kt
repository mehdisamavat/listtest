package com.example.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubCategoryObject(
    val id: String,
    val title: String
):Parcelable