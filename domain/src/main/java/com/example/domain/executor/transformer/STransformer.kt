package com.example.domain.executor.transformer

import io.reactivex.SingleTransformer


abstract class STransformer<T> : SingleTransformer<T, T>