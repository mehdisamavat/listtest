package com.example.domain.repository

import com.example.domain.entity.ResponseObject
import io.reactivex.Single

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
interface CategoryRepository {
    fun getCategory(): Single<ResponseObject>
}