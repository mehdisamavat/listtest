package com.example.data.network

import com.example.data.entity.ResponseEntity
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by Mehdi Samavat on 5/17/20.
 */
interface ApiService {

    @GET("Category")
    fun getCategory(
    ): Single<ResponseEntity>
}