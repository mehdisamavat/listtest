package com.example.data.network

import dagger.Module
import dagger.Provides
import dagger.Reusable

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
@Module
class NetworkModule {

    @Provides
    @Reusable
    fun postDataService(dataServiceFactory: DataServiceFactory): ApiService =
        dataServiceFactory.create(ApiService::class.java)


}