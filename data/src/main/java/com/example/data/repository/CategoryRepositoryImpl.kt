package com.example.data.repository

import com.example.data.extention.toObjectResponse
import com.example.data.repository.datasource.CategoryDataSource
import com.example.domain.entity.ResponseObject
import com.example.domain.repository.CategoryRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
class CategoryRepositoryImpl @Inject constructor(private val categoryDataSource: CategoryDataSource) :
    CategoryRepository {
    override fun getCategory(): Single<ResponseObject> {
        return categoryDataSource.getCategory().map { it.toObjectResponse() }
    }
}