package com.example.data.repository.datasource

import com.example.data.entity.ResponseEntity
import io.reactivex.Single

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
interface CategoryDataSource {
    fun getCategory(): Single<ResponseEntity>
}