package com.example.data.repository.datasource

import com.example.data.entity.ResponseEntity
import com.example.data.network.ApiService
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
class CategoryDataSourceImpl @Inject constructor(private val apiService: ApiService) :
    CategoryDataSource {
    override fun getCategory(): Single<ResponseEntity> {
        return apiService.getCategory()
    }
}