package com.example.data.extention

import com.example.data.entity.ResponseEntity
import com.example.domain.entity.ResponseObject
import com.example.domain.entity.ResultObject
import com.example.domain.entity.SubCategoryObject

/**
 * Created by Mehdi Samavat on 5/18/20.
 */


fun ResponseEntity.toObjectResponse(): ResponseObject {
    return ResponseObject(message, paging, result.map {
        ResultObject(
            it.id,
            it.subCategories.map { subCategory ->
                SubCategoryObject(
                    subCategory.id,
                    subCategory.title
                )
            },
            it.title
        )
    }, statusCode, succeeded, uiMessage)
}