package com.example.data.entity

data class Result(
    val id: String,
    val subCategories: List<SubCategory>,
    val title: String
)