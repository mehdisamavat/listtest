package com.example.data.entity

data class SubCategory(
    val id: String,
    val title: String
)