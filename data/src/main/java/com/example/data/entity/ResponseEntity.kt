package com.example.data.entity

data class ResponseEntity(
    val message: Any?=null,
    val paging: Any?=null,
    val result: List<Result>,
    val statusCode: String,
    val succeeded: Boolean,
    val uiMessage: Any?=null
)