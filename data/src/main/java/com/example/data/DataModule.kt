package com.example.data

import com.example.data.network.NetworkModule
import com.example.data.repository.CategoryRepositoryImpl
import com.example.data.repository.datasource.CategoryDataSource
import com.example.data.repository.datasource.CategoryDataSourceImpl
import com.example.domain.repository.CategoryRepository
import dagger.Binds
import dagger.Module
import dagger.Reusable

/**
 * Created by Mehdi Samavat on 5/18/20.
 */
@Module(includes = [NetworkModule::class])
abstract class DataModule {
    @Binds
    abstract fun provideCategoryDataSource(categoryDataSourceImpl:CategoryDataSourceImpl): CategoryDataSource
    @Binds
    abstract fun provideCategoryRepository(categoryRepositoryImpl:CategoryRepositoryImpl): CategoryRepository


}