package com.example.listtest

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * Created by Mehdi Samavat on 5/16/20.
 */
class App : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }
}