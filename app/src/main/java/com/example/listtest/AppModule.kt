package com.example.listtest

import android.app.Application
import com.example.data.DataModule
import com.example.domain.DomainModule
import com.example.presentation.PresentationModule
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjectionModule

/**
 * Created by Mehdi Samavat on 5/16/20.
 */
@Module(includes = [AndroidInjectionModule::class, PresentationModule::class, DataModule::class,DomainModule::class])
abstract class AppModule {
    @Binds
    abstract fun provideApplication(app: App):Application
}