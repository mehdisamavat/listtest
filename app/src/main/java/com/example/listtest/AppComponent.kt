package com.example.listtest

import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by Mehdi Samavat on 5/16/20.
 */
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent :AndroidInjector<App>{
    @Component.Factory
    interface Factory:AndroidInjector.Factory<App>
}